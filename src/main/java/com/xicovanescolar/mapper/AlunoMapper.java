package com.xicovanescolar.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.xicovanescolar.domain.Aluno;
import com.xicovanescolar.domain.Link;
import com.xicovanescolar.entity.AlunoEntity;

@Component
public final class AlunoMapper {

	private AlunoMapper() {
	}

	public AlunoEntity buildAlunoEntity(Aluno aluno) {
		AlunoEntity alunoEntity = new AlunoEntity();

		alunoEntity.setId(aluno.getId());
		alunoEntity.setNome(aluno.getNome());
		alunoEntity.setRg(aluno.getRg());
		alunoEntity.setCpf(aluno.getCpf());
		alunoEntity.setIdCarro(aluno.getIdCarro());
		alunoEntity.setIdEscola(aluno.getIdEscola());
		alunoEntity.setIdResponsavel(aluno.getIdResponsavel());

		return alunoEntity;
	}

	public Aluno buildAluno(AlunoEntity alunoEntity) {
		List<Link> links = new ArrayList<Link>();
		Aluno aluno = new Aluno();

		aluno.setId(alunoEntity.getId());
		aluno.setNome(alunoEntity.getNome());
		aluno.setRg(alunoEntity.getRg());
		aluno.setCpf(alunoEntity.getCpf());
		
		aluno.setIdEscola(alunoEntity.getIdEscola());
		aluno.setIdResponsavel(alunoEntity.getIdResponsavel());
		aluno.setIdCarro(alunoEntity.getIdCarro());
		
		
		if (alunoEntity.getIdEscola() != null) {
			Link link = new Link();
			link.setHref("/xicovanescolar/escola/"+alunoEntity.getIdEscola());
			link.setType("GET");
			
			links.add(link);
		}
		
		if (alunoEntity.getIdCarro() != null) {
			Link link = new Link();
			link.setHref("/xicovanescolar/carro/"+alunoEntity.getIdCarro());
			link.setType("GET");
			
			links.add(link);
		}
		
		if (alunoEntity.getIdResponsavel() != null) {
			Link link = new Link();
			link.setHref("/xicovanescolar/responsavel/"+alunoEntity.getIdResponsavel());
			link.setType("GET");
			
			links.add(link);
		}
		
		
		aluno.setLinks(links);

		return aluno;
	}

	public List<Aluno> buildAlunos(List<AlunoEntity> alunoEntities) {
		List<Aluno> alunos = new ArrayList<Aluno>();

		alunoEntities.forEach(alunoEntity -> {
			alunos.add(this.buildAluno(alunoEntity));
		});

		return alunos;
	}

}