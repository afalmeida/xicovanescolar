package com.xicovanescolar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.domain.Endereco;
import com.xicovanescolar.service.EnderecoService;

//@RestController
//@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class EnderecoController {

	@Autowired
	private EnderecoService enderecoService;

	@RequestMapping(value = "/pessoa/{pessoaId}/endereco", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Endereco> pessoaEndereco(@PathVariable("pessoaId") Integer pessoaId) {

		try {
			Endereco endereco = enderecoService.getEnderecoByPessoa(pessoaId);

			if (endereco == null) {
				return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
			
			} else {
				return new ResponseEntity<Endereco>(endereco, HttpStatus.OK);
			}

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Endereco>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/pessoa/{pessoaId}/endereco", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Endereco> salvaPessoaEndereco(@PathVariable("pessoaId") Integer pessoaId,
			@RequestBody @Valid final Endereco endereco) {

		try {
			enderecoService.salvarEnderecoByPessoa(endereco, pessoaId);

			return new ResponseEntity<Endereco>(endereco, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Endereco>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/pessoa/{pessoaId}/endereco/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Endereco> editarPessoaEndereco(
			@PathVariable("pessoaId") Integer pessoaId,
			@PathVariable("id") Integer id,
			@RequestBody @Valid final Endereco endereco) {

		try {
			endereco.setId(id);
			enderecoService.salvarEnderecoByPessoa(endereco, pessoaId);

			return new ResponseEntity<Endereco>(HttpStatus.NO_CONTENT);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Endereco>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
