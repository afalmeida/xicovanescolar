package com.xicovanescolar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.entity.ResponsavelEntity;
import com.xicovanescolar.repository.ResponsavelRepository;
import com.xicovanescolar.service.ResponsavelService;

@RestController
@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResponsavelController {

	@Autowired
	private ResponsavelRepository responsavelService;

	@RequestMapping(value = "/responsavel", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResponsavelEntity>> alunos() {
		try {
			List<ResponsavelEntity> responsaveis = responsavelService.findAll();

			if (responsaveis.isEmpty()) {
				return new ResponseEntity<List<ResponsavelEntity>>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<List<ResponsavelEntity>>(responsaveis, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<List<ResponsavelEntity>>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<List<ResponsavelEntity>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/responsavel/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ResponsavelEntity> getAluno(@PathVariable("id") Integer id) {

		try {
			ResponsavelEntity responsavel = responsavelService.getOne(id);

			return new ResponseEntity<ResponsavelEntity>(responsavel, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<ResponsavelEntity>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<ResponsavelEntity>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}