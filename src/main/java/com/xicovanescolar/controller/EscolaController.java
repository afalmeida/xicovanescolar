package com.xicovanescolar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.entity.EscolaEntity;
import com.xicovanescolar.repository.EscolaRepository;
import com.xicovanescolar.service.EscolaService;

@RestController
@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class EscolaController {

	@Autowired
	private EscolaRepository escolaService;

	@RequestMapping(value = "/escola", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<EscolaEntity>> alunos() {
		try {
			List<EscolaEntity> escolas = escolaService.findAll();

			if (escolas.isEmpty()) {
				return new ResponseEntity<List<EscolaEntity>>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<List<EscolaEntity>>(escolas, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<List<EscolaEntity>>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<List<EscolaEntity>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/escola/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<EscolaEntity> getAluno(@PathVariable("id") Integer id) {

		try {
			EscolaEntity escola = escolaService.findById(id).get();

			return new ResponseEntity<EscolaEntity>(escola, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<EscolaEntity>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<EscolaEntity>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}