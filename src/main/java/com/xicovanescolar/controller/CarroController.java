package com.xicovanescolar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.entity.CarroEntity;
import com.xicovanescolar.repository.CarroRepository;
import com.xicovanescolar.service.CarroService;

@RestController
@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class CarroController {

	@Autowired
	private CarroRepository carroService;

	@RequestMapping(value = "/carro", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<CarroEntity>> alunos() {
		try {
			List<CarroEntity> carros = carroService.findAll();

			if (carros.isEmpty()) {
				return new ResponseEntity<List<CarroEntity>>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<List<CarroEntity>>(carros, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<List<CarroEntity>>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<List<CarroEntity>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/carro/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<CarroEntity> getAluno(@PathVariable("id") Integer id) {

		try {
			CarroEntity carro = carroService.getOne(id);

			return new ResponseEntity<CarroEntity>(carro, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<CarroEntity>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<CarroEntity>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}