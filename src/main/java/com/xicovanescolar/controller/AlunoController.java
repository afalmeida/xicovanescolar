package com.xicovanescolar.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.domain.Aluno;
import com.xicovanescolar.service.AlunoService;

@RestController
@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class AlunoController {

	@Autowired
	private AlunoService alunoService;

	@RequestMapping(value = "/aluno", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Aluno>> alunos() {
		try {
			List<Aluno> alunos = alunoService.getAlunos();

			if (alunos.isEmpty()) {
				return new ResponseEntity<List<Aluno>>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<List<Aluno>>(alunos, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<List<Aluno>>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<List<Aluno>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/aluno/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Aluno> getAluno(@PathVariable("id") Integer id) {

		try {
			Aluno aluno = alunoService.getAluno(id);

			return new ResponseEntity<Aluno>(aluno, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Aluno>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Aluno>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/aluno", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Aluno> saveAluno(@RequestBody @Valid final Aluno aluno) {

		try {
			alunoService.salvarAluno(aluno);

			return new ResponseEntity<Aluno>(HttpStatus.CREATED);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Aluno>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Aluno>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/aluno/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Aluno> editarAluno(@PathVariable("id") Integer id,
			@RequestBody @Valid final Aluno aluno) {

		try {
			aluno.setId(id);
			alunoService.editarAluno(aluno);

			return new ResponseEntity<Aluno>(HttpStatus.CREATED);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Aluno>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Aluno>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/aluno/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Aluno> deletarAluno(@PathVariable("id") Integer id) {

		try {
			alunoService.deletarAluno(id);

			return new ResponseEntity<Aluno>(HttpStatus.CREATED);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Aluno>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Aluno>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}