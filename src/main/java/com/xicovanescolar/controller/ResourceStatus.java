package com.xicovanescolar.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/xicovanescolar", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResourceStatus {
	
//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public @ResponseBody ResponseEntity<String> status() {
//		
//		return new ResponseEntity<String>("OK", HttpStatus.OK);
//	}
	
	@RequestMapping(value = "/resource-status", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> statusX() {
		
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}

}
