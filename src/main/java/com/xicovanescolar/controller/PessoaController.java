package com.xicovanescolar.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xicovanescolar.domain.Pessoa;
import com.xicovanescolar.service.PessoaService;

//@RestController
//@RequestMapping(value = "/xicovanescolar/pessoa", produces = MediaType.APPLICATION_JSON_VALUE)
public class PessoaController {

	@Autowired
	private PessoaService pessoaService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Pessoa>> pessoas() {

		try {
			List<Pessoa> pessoas = pessoaService.getPessoas();
			
			if (pessoas.isEmpty()) {
				return new ResponseEntity<List<Pessoa>>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<List<Pessoa>>(pessoas, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<List<Pessoa>>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<List<Pessoa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Pessoa> pessoa(@PathVariable("id") Integer id) {

		try {
			Pessoa pessoa = pessoaService.getPessoa(id);

			return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Pessoa> savePessoa(@RequestBody @Valid final Pessoa pessoa) {

		try {
			pessoaService.salvarPessoa(pessoa);

			return new ResponseEntity<Pessoa>(HttpStatus.CREATED);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Pessoa> editarPessoa(@PathVariable("id") Integer id,
			@RequestBody @Valid final Pessoa pessoa) {

		try {
			pessoa.setId(id);
			pessoaService.editarPessoa(pessoa);

			return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);

		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Pessoa> deletarPessoa(@PathVariable("id") Integer id) {
		try {
			pessoaService.deletarPessoa(id);

			return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);
		} catch (javax.persistence.EntityNotFoundException e) {
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}