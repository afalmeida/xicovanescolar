package com.xicovanescolar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XicovanescolarApplication {

	public static void main(String[] args) {
		SpringApplication.run(XicovanescolarApplication.class, args);
	}

}
