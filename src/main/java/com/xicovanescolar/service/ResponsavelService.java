package com.xicovanescolar.service;

import java.util.List;

import com.xicovanescolar.entity.ResponsavelEntity;

public interface ResponsavelService {

	public List<ResponsavelEntity> getResponsaveis();

	public ResponsavelEntity getResponsavel(Integer id);

}
