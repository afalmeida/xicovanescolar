package com.xicovanescolar.service;

import java.util.List;

import com.xicovanescolar.domain.Aluno;

public interface AlunoService {
	
	public Aluno getAluno(Integer id);
	
	public List<Aluno> getAlunos();
	
	public void salvarAluno(Aluno aluno);
	
	public void editarAluno(Aluno aluno);
	
	public void deletarAluno(Integer id);

}