package com.xicovanescolar.service;

import com.xicovanescolar.domain.Endereco;

public interface EnderecoService {
	
	public Endereco getEnderecoByPessoa(Integer pessoaId);
	public void salvarEnderecoByPessoa(Endereco endereco, Integer pessoaId);
	
}