package com.xicovanescolar.service;

import java.util.List;

import com.xicovanescolar.entity.CarroEntity;

public interface CarroService {

	public List<CarroEntity> getCarros();

	public CarroEntity getCarro(Integer id);

}