package com.xicovanescolar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xicovanescolar.domain.Aluno;
import com.xicovanescolar.mapper.AlunoMapper;
import com.xicovanescolar.repository.AlunoRepository;

@Service
public class AlunoServiceImpl implements AlunoService {
	
	@Autowired
	private AlunoRepository alunoRepository;
	
	@Autowired
	private AlunoMapper alunoMapper;

	@Override
	public Aluno getAluno(Integer id) {
		return alunoMapper.buildAluno(alunoRepository.getOne(id));
	}

	@Override
	public List<Aluno> getAlunos() {
		return alunoMapper.buildAlunos(alunoRepository.findAll());
	}

	@Override
	public void salvarAluno(Aluno aluno) {
		alunoRepository.save(alunoMapper.buildAlunoEntity(aluno));
		
	}

	@Override
	public void editarAluno(Aluno aluno) {
		alunoRepository.save(alunoMapper.buildAlunoEntity(aluno));
		
	}

	@Override
	public void deletarAluno(Integer id) {
		alunoRepository.deleteById(id);
		
	}



}
