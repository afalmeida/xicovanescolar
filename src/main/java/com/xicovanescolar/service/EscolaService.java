package com.xicovanescolar.service;

import java.util.List;

import com.xicovanescolar.entity.EscolaEntity;

public interface EscolaService {

	public EscolaEntity getEscola(Integer id);

	public List<EscolaEntity> getEscolas();

}
