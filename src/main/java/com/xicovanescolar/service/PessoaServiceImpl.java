package com.xicovanescolar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xicovanescolar.domain.Pessoa;
import com.xicovanescolar.repository.PessoaRepository;

@Service
public class PessoaServiceImpl implements PessoaService {
	
	@Autowired
	private PessoaRepository pessoaRepository;

	@Override
	public Pessoa getPessoa(Integer id) {
		return pessoaRepository.findById(id).get();
		
	}
	
	@Override
	public List<Pessoa> getPessoas() {
		return pessoaRepository.findAll();
		
	}

	@Override
	public void salvarPessoa(Pessoa pessoa) {
		pessoaRepository.save(pessoa);
		
	}

	@Override
	public void editarPessoa(Pessoa pessoa) {
		pessoaRepository.save(pessoa);
		
	}

	@Override
	public void deletarPessoa(Integer id) {
		pessoaRepository.deleteById(id);
		
	}
}