package com.xicovanescolar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xicovanescolar.domain.Endereco;
import com.xicovanescolar.domain.Pessoa;
import com.xicovanescolar.repository.EnderecoRepository;

@Service
public class EnderecoServiceImpl implements EnderecoService {
	
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Override
	public Endereco getEnderecoByPessoa(Integer pessoaId) {
		return enderecoRepository.findEnderecoEntityByPessoaId(pessoaId);
	}

	@Override
	public void salvarEnderecoByPessoa(Endereco endereco, Integer pessoaId) {
		
//		Pessoa pessoa = new Pessoa();
//		pessoa.setId(pessoaId);
//		endereco.setPessoa(pessoa);
		
		endereco.setPessoaId(pessoaId);
		
		enderecoRepository.save(endereco);
		
	}


}
