package com.xicovanescolar.service;

import java.util.List;

import com.xicovanescolar.domain.Pessoa;

public interface PessoaService {
	
	public Pessoa getPessoa(Integer id);
	
	public List<Pessoa> getPessoas();
	
	public void salvarPessoa(Pessoa pessoa);
	
	public void editarPessoa(Pessoa pessoa);
	
	public void deletarPessoa(Integer id);
	
}