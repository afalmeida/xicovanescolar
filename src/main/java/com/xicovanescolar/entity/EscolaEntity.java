package com.xicovanescolar.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ESCOLA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EscolaEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5009622096186802253L;

	@Id
	private Integer id;
	
	@Column(name = "nome")
	private String nome;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
