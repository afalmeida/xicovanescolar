package com.xicovanescolar.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "CARRO")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CarroEntity implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5009622096186802253L;

	@Id
	private Integer id;
	
	@Column(name = "MODELO")
	private String modelo;

	@Column(name = "PLACA")
	private String placa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

}
