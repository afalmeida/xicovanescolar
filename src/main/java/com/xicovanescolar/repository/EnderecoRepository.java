package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.domain.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {
	
	
	public Endereco findEnderecoEntityByPessoaId(Integer pessoaId);

}
