package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.domain.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {}