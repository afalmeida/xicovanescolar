package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.entity.EscolaEntity;

public interface EscolaRepository extends JpaRepository<EscolaEntity, Integer> {

}
