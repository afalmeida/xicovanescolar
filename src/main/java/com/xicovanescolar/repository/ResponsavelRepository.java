package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.entity.ResponsavelEntity;

public interface ResponsavelRepository extends JpaRepository<ResponsavelEntity, Integer> {

}
