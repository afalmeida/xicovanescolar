package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.entity.AlunoEntity;

public interface AlunoRepository extends JpaRepository<AlunoEntity, Integer> {

}
