package com.xicovanescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xicovanescolar.entity.CarroEntity;

public interface CarroRepository extends JpaRepository<CarroEntity, Integer> {

}
