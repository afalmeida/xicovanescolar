package com.xicovanescolar.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xicovanescolar.entity.CarroEntity;
import com.xicovanescolar.entity.EscolaEntity;
import com.xicovanescolar.entity.ResponsavelEntity;
import com.xicovanescolar.repository.CarroRepository;
import com.xicovanescolar.repository.EscolaRepository;
import com.xicovanescolar.repository.ResponsavelRepository;

@Configuration
public class MockConfig {
	
	
	@Autowired
	private EscolaRepository escolaRepository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
	private CarroRepository carroRepository;
	
	
	
	@Bean(name = "mock") 
	public void mock() {
		
	
		EscolaEntity escolaEntity1 = new EscolaEntity();
		escolaEntity1.setId(27);
		escolaEntity1.setNome("Escola Joao de Camargo");
		
		EscolaEntity escolaEntity2 = new EscolaEntity();
		escolaEntity2.setId(28);
		escolaEntity2.setNome("Escola Luiz Ribeiro Filho");
		

		
		ResponsavelEntity responsavelEntity1 = new ResponsavelEntity();
		responsavelEntity1.setId(25);
		responsavelEntity1.setCpf("1123");
		responsavelEntity1.setNome("Roberto Silva");
		responsavelEntity1.setRg("1234");
		
		ResponsavelEntity responsavelEntity2 = new ResponsavelEntity();
		responsavelEntity2.setId(26);
		responsavelEntity2.setCpf("1123");
		responsavelEntity2.setNome("Wesley Pereira");
		responsavelEntity2.setRg("1234");
		
		ResponsavelEntity responsavelEntity3 = new ResponsavelEntity();
		responsavelEntity3.setId(27);
		responsavelEntity3.setCpf("1123");
		responsavelEntity3.setNome("Lucas Silva Silva");
		responsavelEntity3.setRg("1234");

		
		CarroEntity carroEntity = new CarroEntity();
		carroEntity.setId(25);
		carroEntity.setModelo("VAN 2012");
		carroEntity.setPlaca("FBP-0303");
		
		CarroEntity carroEntity2 = new CarroEntity();
		carroEntity2.setId(26);
		carroEntity2.setModelo("Kombi 2012");
		carroEntity2.setPlaca("FBP-0303");
		
		
		
		escolaRepository.save(escolaEntity1);
		escolaRepository.save(escolaEntity2);
		
		carroRepository.save(carroEntity);
		
		carroRepository.save(carroEntity2);
		
		responsavelRepository.save(responsavelEntity1);
		responsavelRepository.save(responsavelEntity2);
		responsavelRepository.save(responsavelEntity3);
	}

}
