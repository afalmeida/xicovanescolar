package com.xicovanescolar.configuration;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerSac",
        transactionManagerRef = "transactionManagerSac",
        basePackages = {"com.xicovanescolar.repository"})
public class DataSourceConfig {

	
	@Bean(name = "dataSourceSac")
	public DataSource dataSourceSac() throws NumberFormatException, SQLException {


	

//		properties.put("initialPoolSize", Integer.parseInt(env.getProperty("data-source.sac.initialPoolSize")));
//		properties.put("minPoolSize", Integer.parseInt(env.getProperty("data-source.sac.minPoolSize")));
//		properties.put("maxPoolSize", Integer.parseInt(env.getProperty("data-source.sac.maxPoolSize")));
		
		
//	    driver.classname: oracle.jdbc.driver.OracleDriver
//	    url: 'jdbc:oracle:thin:@dbkistapr.back.b2w:1521/BWKSTPR'
//	    username.b2w:  sac_api_b2w_dml
		
		
		
		
		String conexao = "jdbc:oracle:thin:@apporacle:1521:xe";
	
//		if (StringUtils.isNotBlank(System.getenv("DATA_SOURCE_NAME"))) {
//			conexao = System.getenv("DATA_SOURCE_NAME");
//		}
		
		
		System.out.println("ORACLE URL -= - - - - - - -- - - - -  - - - - -"+ conexao);
		
	
		

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl(conexao);
		dataSource.setUsername("system");
		dataSource.setPassword("oracle");
		//dataSource.setConnectionProperties(properties);


		return dataSource;
	}
		
	@Bean(name = "entityManagerSac")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NumberFormatException, SQLException {
		LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		localContainerEntityManagerFactoryBean.setDataSource(dataSourceSac());
		localContainerEntityManagerFactoryBean.setPackagesToScan("com.xicovanescolar.*");
		localContainerEntityManagerFactoryBean.setPersistenceUnitName("entityManagerSac");
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		localContainerEntityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
		
		
		Properties properties = new Properties();
		properties.put("hibernate.show_sql",true);
		
		properties.put("hibernate.format_sql", "true");
		properties.put("hibernate.enable_lazy_load_no_trans", "true");
		properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		properties.put("hibernate.hbm2ddl.auto", "update");
		
		localContainerEntityManagerFactoryBean.setJpaProperties(properties);
			    
		localContainerEntityManagerFactoryBean.setJpaProperties(properties);
		
		return localContainerEntityManagerFactoryBean;
	}
	
	@Bean(name = "transactionManagerSac")
	public PlatformTransactionManager transactionManager() throws NumberFormatException, SQLException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		
		return transactionManager;
	}
	
//	@Bean
//	public HibernateExceptionTranslator hibernateExceptionTranslator(){
//	    return  new HibernateExceptionTranslator();
//	}

	@Bean(name = "persistenceExceptionTranslationSac")
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
}