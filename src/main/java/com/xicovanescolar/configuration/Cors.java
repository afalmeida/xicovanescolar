package com.xicovanescolar.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class Cors implements Filter {

	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		((HttpServletResponse) response).setHeader("Access-Control-Allow-Origin", "*.*");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Credentials", "true");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Methods",
				"POST, GET, PUT, OPTIONS, PATCH, DELETE");
		((HttpServletResponse) response).setHeader("Access-Control-Allow-Headers",
				"DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Authorization, charset, Content-Encoding, Location, Allow, X-TID, WWW-Authenticate, X-Access-Control-Realm, internalId, Accept-Encoding, Accept-Language, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, Pragma, Referer, X-Preview, log, X-B2W-System, X-B2W-UserId, X-B2W-token, X-Authorization-UserPermissions, Access-Token");
		((HttpServletResponse) response).setHeader("Access-Control-Expose-Headers",
				"DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Authorization, charset, Content-Encoding, Location, Allow, X-TID, WWW-Authenticate, X-Access-Control-Realm, internalId, Accept-Encoding, Accept-Language, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, Pragma, Referer, X-Preview, log, X-B2W-System, X-B2W-UserId, X-B2W-token, X-Authorization-UserPermissions, Access-Token");
	
		chain.doFilter(request, response);
	
	}
}
