Projeto XICO VAN ESCOLAR



1 - Executar o docker-compose up (faz donwload das imagens docker oracle e projeto java)

2 - Testar para ver se api esta ok<br>
    localhost:8080/xicovanescolar/resource-status (retorno tem que ser OK)

3 - Documentação swagger<br>
     http://localhost:8080/swagger-ui.html 
    
**METODO GET**<br>
localhost:8080/xicovanescolar/escola (retorno da lista de escolas)<br>
localhost:8080/xicovanescolar/responsavel (retorno da lista de responsaveis)<br>
localhost:8080/xicovanescolar/carro (retorno da lista de carros)<br>
localhost:8080/xicovanescolar/aluno (retorno da lista de alunos)<br>





**METODO POST**<br>
localhost:8080/xicovanescolar/aluno<br>

**body**:<br>
{
    "nome": "Anderson Almeida",
    "rg": "123433333333333",
    "cpf": "123433333333333",
    "idCarro": 1,
    "idResponsavel": 1,
    "idEscola": 1
} (retorno 204)
