FROM maven:3.6.3-jdk-8

# COPY pom.xml
# RUN mvn clean install;

COPY ./target/xicovanescolar-0.0.1-SNAPSHOT.jar  xicovanescolar-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD java -jar -Doracle.jdbc.timezoneAsRegion=false xicovanescolar-0.0.1-SNAPSHOT.jar 



# docker run -d -p 1521:1521 -p 8083:8083 oracleinanutshell/oracle-xe-11g



# docker build -t xicovanescolar:latest . 
# docker run -d -p 8080:8080 xicovanescolar:latest
#docker run -ti -p 8080:8080 xicovanescolar:latest 