import React, { Component } from 'react';

import { Container, Button } from 'react-bootstrap';
import UserList from './GetUser';
import AddUser from './AddUser';
import axios from 'axios';
const apiUrl = 'http://localhost:8080/xicovanescolar/';

class UserActionApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddUser: false,
      error: null,
      response: {},
      userData: {},
      isEdituser: false,
      isUserDetails:true,
    }

    this.onFormSubmit = this.onFormSubmit.bind(this);

  }

  onCreate() {
    this.setState({ isAddUser: true });
    this.setState({ isUserDetails: false });
  }
  onDetails() {
    this.setState({ isUserDetails: true });
    this.setState({ isAddUser: false });
  }

  onFormSubmit(data) {


    let aluno = {
      nome: data.nome,
      rg: data.rg,
      cpf: data.cpf,
      idResponsavel : data.IdResponsavel,
      idEscola: data.IdEscola,
      idCarro : data.IdCarro
    }

    this.setState({ isAddUser: true });
    this.setState({ isUserDetails: false });
    if (this.state.isEdituser) {
     axios.put(apiUrl + "aluno/" + data.id,aluno).then(result => {
      alert("Aluno Cadastrado");
        this.setState({
          response:result,  
          isAddUser: false,
          isEdituser: false
        })
      });
    } else {
 
     axios.post(apiUrl + 'aluno',aluno).then(result => {
      alert("Aluno Cadastrado");
        this.setState({
          response:result,  
          isAddUser: false,
          isEdituser: false
        })
      });
    }
  
  }

  editUser = userId => {

    this.setState({ isUserDetails: false });
   axios.get(apiUrl + "aluno/" + userId).then(result => {

        this.setState({
          isEdituser: true,
          isAddUser: true,
          userData: result.data         
        });
      },
      (error) => {
        this.setState({ error });
      }
    )
   
  }


  render() {
  
    let userForm;
    if (this.state.isAddUser || this.state.isEditUser) {


      console.log("userData",this.state.userData)

      userForm = <AddUser onFormSubmit={this.onFormSubmit} user={this.state.userData} />
     
    }
  

    return (
      <div className="App">
 <Container>
        <h1 style={{ textAlign: 'center' }}>Cadastro de Alunos</h1>
        <hr></hr>
        {!this.state.isUserDetails && <Button variant="primary" onClick={() => this.onDetails()}>Detalhes Aluno</Button>}
        {!this.state.isAddUser && <Button variant="primary" onClick={() => this.onCreate()}>Adicionar Aluno</Button>}
        <br></br>
        {!this.state.isAddUser && <UserList editUser={this.editUser} />}
        {userForm}
        </Container>
      </div>
    );
  }
}
export default UserActionApp;