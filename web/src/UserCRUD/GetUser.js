import React from 'react';
import { Table, Button } from 'react-bootstrap';
import axios from 'axios';

const apiUrl = 'http://localhost:8080/xicovanescolar';

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      users: [],
      escolas: [],
      responsaveis: [],
      carros: [],
      response: {}

    }
  }

  componentDidMount() {
    axios.get(apiUrl + '/aluno').then(response => response.data).then(
      (result) => {
        this.setState({
          users: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

    axios.get(apiUrl + '/escola').then(response => response.data).then(
      (result) => {
        this.setState({
          escolas: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

    axios.get(apiUrl + '/responsavel').then(response => response.data).then(
      (result) => {
        this.setState({
          responsaveis: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

    axios.get(apiUrl + '/carro').then(response => response.data).then(
      (result) => {
        this.setState({
          carros: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )
  }

  escola(id) {

    console.log("escola id", id)

    const escola = this.state.escolas.filter(escola => escola.id === id)[0];


    // console.log("carros", carros)

    // if (id === 25) {
    //   return "VAN 2012"
    // } else {
    //   return "Kombi 2012"
    // }

    if (escola !== undefined) {
      return escola.nome;
    }
    // let escola = undefined;

    // if (id !== undefined) {
    //   axios.get(apiUrl + '/escola/' + id).then(response => response.data).then(
    //     (result) => {
    //       // this.setState({
    //       //     users:result
    //       // });

    //       escola = result
    //     },
    //     (error) => {
    //       this.setState({ error });
    //     }
    //   )
    // }


    // console.log("escola3333333", escola)
    // if (id === 27) {
    //   return "Escola Joao de Camargo"
    // } else {
    //   return "Escola Luiz Ribeiro Filho"
    // }
  }

  carro(id) {
    console.log("carro id ", id)
    const carro = this.state.carros.filter(carro => carro.id === id)[0];


    // console.log("carros", carros)

    // if (id === 25) {
    //   return "VAN 2012"
    // } else {
    //   return "Kombi 2012"
    // }

    if (carro !== undefined) {
      return carro.modelo;
    }



  }

  responsavel(id) {

    console.log("responsavel", id)
    const responsavel = this.state.responsaveis.filter(responsavel => responsavel.id === id)[0];

    if (responsavel !== undefined) {
      return responsavel.nome;
    }

    // if (id === 25) {
    //   return "Roberto Silva"
    // } else if (id === 26) {
    //   return "Wesley Pereira"
    // } else {
    //   return "Lucas Silva Silva"
    // }
  }



  deleteUser(userId) {

    console.log("dddddd", userId)
    // const { users } = this.state;   
    axios.delete(apiUrl + '/aluno/' + userId).then(result => {
      alert("Aluno Deletado");
      axios.get(apiUrl + '/aluno').then(response => response.data).then(
        (result) => {
          this.setState({
            users: result
          });
        },
        (error) => {
          this.setState({ error });
        }
      )

      this.setState({
        response: result,
        //users:users.filter(user=>user.UserId !== userId)
      });
    });

    axios.get(apiUrl + '/aluno').then(response => response.data).then(
      (result) => {
        this.setState({
          users: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

  }

  render() {
    const { error, users } = this.state;



    console.log("users", users)
    if (error) {
      return (
        <div>Error:{error.message}</div>
      )
    }
    else {
      return (
        <div>

          <Table>
            <thead className="btn-primary">
              <tr>
                <th>Nome</th>
                <th>RG</th>
                <th>CPF</th>
                <th>Responsavel</th>
                <th>Escola</th>
                <th>Carro</th>
              </tr>
            </thead>
            <tbody>
              {users.map(user => (
                <tr key={user.id}>
                  <td>{user.nome}</td>
                  <td>{user.rg}</td>
                  <td>{user.cpf}</td>
                  <td>{this.responsavel(user.idResponsavel)}</td>
                  <td>{this.escola(user.idEscola)}</td>
                  <td>{this.carro(user.idCarro)}</td>
                  <td><Button variant="info" onClick={() => this.props.editUser(user.id)}>Edit</Button>  &nbsp;&nbsp;&nbsp;
                          <Button variant="danger" onClick={() => this.deleteUser(user.id)}>Delete</Button>

                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      )
    }
  }
}

export default UserList;