import React from 'react';
import { Row, Form, Col, Button } from 'react-bootstrap';


import axios from 'axios';

const apiUrl = 'http://localhost:8080/xicovanescolar';

class AddUser extends React.Component {
  constructor(props) {
    super(props);

    this.initialState = {
      id: '',
      nome: '',
      rg: '',
      cpf: '',
      IdEscola: '',
      IdCarro: '',
      IdResponsavel: '',
      escolas: undefined,
      responsaveis: undefined,
      carros: undefined
    }


    if (props.user.id) {

      console.log(props.user)
      this.state = props.user
    } else {
      this.state = this.initialState;
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  componentDidMount() {
    axios.get(apiUrl + '/escola').then(response => response.data).then(
      (result) => {
        this.setState({
          escolas: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

    axios.get(apiUrl + '/responsavel').then(response => response.data).then(
      (result) => {
        this.setState({
          responsaveis: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )

    axios.get(apiUrl + '/carro').then(response => response.data).then(
      (result) => {
        this.setState({
          carros: result
        });
      },
      (error) => {
        this.setState({ error });
      }
    )
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;


    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onFormSubmit(this.state);
    this.setState(this.initialState);
  }
  render() {
    let pageTitle;
    let actionStatus;
    const rowsEscolas = []
    const rowsCarros = []
    const rowsResponsaveis = []
    const escolas = this.state.escolas;
    const carros = this.state.carros;
    const responsaveis = this.state.responsaveis;

    if (this.state.Id) {

      pageTitle = <h2>Editar Aluno</h2>
      actionStatus = <b>Update</b>
    } else {
      pageTitle = <h2>Adicionar Aluno</h2>
      actionStatus = <b>Salvar Aluno</b>
    }


    

    if (this.state.Id) {

      pageTitle = <h2>Editar Aluno</h2>
      actionStatus = <b>Update</b>
    } else {
      pageTitle = <h2>Adicionar Aluno</h2>
      actionStatus = <b>Salvar Aluno</b>
    }

    console.log("escolas", escolas)
    console.log("carros", carros)
    console.log("responsaveis", responsaveis)

    if (escolas != null) {
      escolas.map((escola, key) => {
        rowsEscolas.push(
          <option key={key} value={escola.id}>{escola.nome}</option>
        )
      })
    }

    if (carros != null) {
      carros.map((carro, key) => {
        rowsCarros.push(
          <option key={key} value={carro.id}>{carro.modelo}</option>
        )
      })
    }

    if (responsaveis != null) {
      responsaveis.map((responsavel, key) => {
        rowsResponsaveis.push(
          <option key={key} value={responsavel.id}>{responsavel.nome}</option>
        )
      })
    }

    return (
      <div>
        <h2> {pageTitle}</h2>
        <Row>
          <Col sm={7}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="nome">
                <Form.Label>Nome</Form.Label>
                <Form.Control
                  type="text"
                  name="nome"
                  value={this.state.nome}
                  onChange={this.handleChange}
                  placeholder="Nome" />
              </Form.Group>
              <Form.Group controlId="rg">
                <Form.Label>RG</Form.Label>
                <Form.Control
                  type="text"
                  name="rg"
                  value={this.state.rg}
                  onChange={this.handleChange}
                  placeholder="Rg" />
              </Form.Group>
              <Form.Group controlId="cpf">
                <Form.Label>CPF</Form.Label>
                <Form.Control
                  type="text"
                  name="cpf"
                  value={this.state.cpf}
                  onChange={this.handleChange}
                  placeholder="Cpf" />
              </Form.Group>

              <Form.Group controlId="IdResponsavel">
                <Form.Label>Responsavel</Form.Label>

                <Form.Control as="select" name="IdResponsavel"
                  value={this.state.IdResponsavel}
                  onChange={(e) => this.handleChange(e)}>
                  <option value="">Selecione....</option>
                  {/* <option value="25">Roberto Silva</option>
                  <option value="26">Wesley Pereira</option>
                  <option value="27">Lucas Silva Silva</option> */}
                  {rowsResponsaveis}
                </Form.Control>

              </Form.Group>

              <Form.Group controlId="IdEscola">
                <Form.Label>Escola</Form.Label>

                <Form.Control as="select" name="IdEscola"
                  value={this.state.IdEscola}
                  onChange={(e) => this.handleChange(e)}>
                  <option value="">Selecione....</option>
                  {/* <option value="27">Escola Joao de Camargo</option>
                  <option value="28">Escola Luiz Ribeiro Filho</option> */}

                  {rowsEscolas}
                </Form.Control>

              </Form.Group>
              <Form.Group controlId="IdCarro">
                <Form.Label>Carro</Form.Label>

                <Form.Control as="select" name="IdCarro"
                  value={this.state.IdCarro}
                  onChange={(e) => this.handleChange(e)}>
                  <option value="">Selecione....</option>
                  {/* <option value="25">VAN 2012</option>
                  <option value="26">Kombi 2012</option> */}

                  {rowsCarros}
                </Form.Control>

              </Form.Group>



              <Form.Group>
                <Form.Control type="hidden" name="UserId" value={this.state.Id} />
                <Button variant="success" type="submit">{actionStatus}</Button>

              </Form.Group>
            </Form>
          </Col>
        </Row>
      </div >
    )
  }
}

export default AddUser;